﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Psychic.Models
{
    public class BaseRequest
    {
        public int Id { get; set; }
        public int? Number { get; set; }
        public DateTime DateTime { get; set; }
    }
}
