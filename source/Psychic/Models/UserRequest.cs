﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Psychic.Models
{
    public class UserRequest: BaseRequest
    {
        public User User { get; set; }
    }
}
