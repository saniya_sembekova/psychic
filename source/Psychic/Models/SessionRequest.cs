﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Psychic.Models
{
    public class SessionRequest
    {
        public int Id { get; set; }
        public int SessionId { get; set; }
        public User User { get; set; }
        public UserRequest UserRequest { get; set; }
        public List<ExtrasensRequest> ExtrasensRequests { get; set; }
    }
}
