﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Psychic.Models
{
    public class UserSession
    {
        public int Id { get; set; }
        public User User { get; set; }
        public List<SessionRequest> Requests { get; set; }
    }
}
