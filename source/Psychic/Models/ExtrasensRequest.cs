﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Psychic.Models
{
    public class ExtrasensRequest: BaseRequest
    {
        public Extrasens Extrasens { get; set; }
        public bool IsRight { get; set; }
    }
}
