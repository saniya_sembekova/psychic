﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Psychic.Models;
using Psychic.Helpers;

namespace Psychic.Controllers
{
    [Route("api/[controller]")]
    public class ValuesController : Controller
    {
        private readonly GuessProvider _guessProvider = new GuessProvider(); 

        [HttpGet]
        public IActionResult Get()
        {
            return new OkObjectResult(DataBase.Extrasenses);
        }

        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }
        
        [HttpPost(("session"))]
        public IActionResult CreateSession()
        {
            User newUser = DataBase.CreateUser();
            UserSession newSession = DataBase.CreateSession(newUser);
            DataBase.Users.Add(newUser);
            DataBase.Sessions.Add(newSession);
            return new OkObjectResult(newSession);
        }

        [HttpPost("request")]
        public IActionResult CreateRequest([FromBody]SessionRequest request)
        {
            var guesses = _guessProvider.Get();
            SessionRequest newRequest = DataBase.CreateSessionRequest(request, guesses);
            DataBase.SessionRequests.Add(newRequest);
            var currentSession = DataBase.Sessions.FirstOrDefault(s => s.Id == request.SessionId);
            currentSession.Requests.Add(newRequest);
            return new OkObjectResult(newRequest);
        }

        [HttpPut()]
        public IActionResult Put([FromBody]SessionRequest request)
        {
            var session = DataBase.Sessions.FirstOrDefault(s => s.Id == request.SessionId);
            var existRequest = session.Requests.FirstOrDefault(r => r.Id == request.Id);
            request.UserRequest.Id = DataBase.UserRequestCounter + 1;
            request.UserRequest.DateTime = DateTime.Now;
            DataBase.UserRequestCounter++;
            existRequest.UserRequest = request.UserRequest;
            foreach (var guess in existRequest.ExtrasensRequests)
            {
                if (guess.Number == existRequest.UserRequest.Number)
                {
                    guess.IsRight = true;
                    guess.Extrasens.Level++;
                } else
                {
                    if (guess.Extrasens.Level > 0)
                    {
                        guess.Extrasens.Level--;
                    }
                }
            }

            return new OkObjectResult(session);
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
