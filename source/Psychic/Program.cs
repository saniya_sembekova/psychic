﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace Psychic
{
    public class Program
    {
        public static void Main(string[] args)
        {
            DataBaseStart();
            BuildWebHost(args).Run();            
        }

        private static void DataBaseStart()
        {
            DataBase.Extrasenses = new List<Models.Extrasens>();
            DataBase.Users = new List<Models.User>();
            DataBase.Sessions = new List<Models.UserSession>();
            DataBase.UserRequests = new List<Models.UserRequest>();
            DataBase.ExtrasensRequests = new List<Models.ExtrasensRequest>();
            DataBase.SessionRequests = new List<Models.SessionRequest>();

            string[] names = new string[] {
                "Joey",
                "Rachel",
                "Ross",
                "Monica",
                "Phoebe",
                "Chandler",
                "Alex",
                "Meredit",
                "April",
                "Derek"
            };
            for (int i = 1; i < 10; i++)
            {
                DataBase.Extrasenses.Add(new Models.Extrasens
                {
                    Id = i,
                    Name = names[i - 1],
                    Level = 0
                });
            }
        }

        public static IWebHost BuildWebHost(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>()
                .Build();
    }
}
