﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Psychic.Models;

namespace Psychic.Helpers
{
    public class GuessProvider
    {
        public List<ExtrasensRequest> Get()
        {
            int countOfExtrasens = GetNum(10, true);
            List<ExtrasensRequest> result = new List<ExtrasensRequest>();
            for (int i = 1; i <= countOfExtrasens; i++)
            {
                var extrasens = DataBase.Extrasenses.FirstOrDefault(e => e.Id == i);
                result.Add(new ExtrasensRequest
                {
                    Id = DataBase.ExtrasensRequestCounter + 1,
                    Number = GetNum(99, false),
                    DateTime = DateTime.Now,
                    Extrasens = extrasens
                });
                DataBase.ExtrasensRequestCounter++;
            }
            DataBase.ExtrasensRequests.AddRange(result);
            return result;
        }
        private int GetNum(int maxNum, bool isCount)
        {
            Random random = new Random();
            int number = 0;
            if (isCount)
            {
                while (number < 2)
                {
                    number = random.Next(maxNum);
                }
            }
            else
            {
                while (number < 10)
                {
                    number = random.Next(maxNum);
                }
            }
            return number;
        }
        
    }
}
