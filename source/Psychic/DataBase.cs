﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Psychic.Models;

namespace Psychic
{
    public static class DataBase
    {
        public static int UserCounter { get; set; }
        public static int UserRequestCounter { get; set; }
        public static int ExtrasensCounter { get; set; }
        public static int ExtrasensRequestCounter { get; set; }
        public static int SessionRequestCounter { get; set; }
        public static int SessionCounter { get; set; }

        public static List<User> Users { get; set; }
        public static List<UserRequest> UserRequests { get; set; }
        public static List<Extrasens> Extrasenses { get; set; }
        public static List<ExtrasensRequest> ExtrasensRequests { get; set; }
        public static List<SessionRequest> SessionRequests { get; set; }
        public static List<UserSession> Sessions { get; set; }

        public static UserSession CreateSession(User newUser)
        {
            var newSession = new UserSession
            {
                Id = SessionCounter + 1,
                User = newUser,
                Requests = new List<SessionRequest>()
            };
            SessionCounter++;
            return newSession;
        }

        public static User CreateUser()
        {
            var newUser = new User
            {
                Id = UserCounter + 1
            };
            UserCounter++;
            return newUser;
        }

        public static SessionRequest CreateSessionRequest(SessionRequest request, List<ExtrasensRequest> guesses)
        {
            var result = new SessionRequest
            {
                Id = SessionRequestCounter + 1,
                SessionId = request.SessionId,
                User = request.User,
                ExtrasensRequests = guesses
            };
            SessionRequestCounter++;
            return result;
        }
    }
}
