export class UserSession {
    id: number;
    user: User;
    requests: number[];
}

export class SessionRequest {
    constructor(
        public sessionId: number,
        public user: User
    ) {}
    id: number;
    userRequest: UserRequest;
    extrasensRequests: ExtrasensRequest[]
}

export class User {
    id: number;
}

export class UserRequest {
    constructor(
        public user: User,
        public number: number
    ) {}
    id: number; 
    dateTime: Date;
}

export class ExtrasensRequest {
    id: number;
    extrasens: Extrasens;
    isRight: boolean;
    numberValue: number; 
    dateTime: Date;
}

export class Extrasens {
    id: number;
    name: string;
    level: number;
}