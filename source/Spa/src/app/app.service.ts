import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import {
  HttpClient,
  HttpParams
} from '@angular/common/http';
import { SessionRequest } from './models/session.models';

@Injectable()
export class AppService {
  apiUrl = '/api/values/';
  constructor(private http: HttpClient) { }

  getAllExtrasenses(): Observable<any> {
    return this.http.get(this.apiUrl);
  }

  startSession(): Observable<any> {
    return this.http
    .post(`${this.apiUrl}session`, "");
  }

  createSessionRequest(request: SessionRequest): Observable<any> {
    return this.http
    .post(`${this.apiUrl}request`, request);
  }

  updateSessionRequest(request: SessionRequest): Observable<any> {
    return this.http
    .put(`${this.apiUrl}`, request);
  }

}
