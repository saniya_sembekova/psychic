import { Component } from '@angular/core';
import { UserSession, SessionRequest, UserRequest, Extrasens } from './models/session.models';
import { AppService } from './app.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { DomSanitizer } from '@angular/platform-browser';
import { MatIconRegistry } from '@angular/material/icon';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
  extrasenses: Extrasens[];
  session: UserSession;
  currentRequest: SessionRequest;
  formGroup: FormGroup;
  
  constructor(
    private fb: FormBuilder,
    private appService: AppService,
    private iconRegistry: MatIconRegistry, 
    private sanitizer: DomSanitizer) {
      this.registryIcons();
      this.fillExtrasenses();
      this.formGroup = this.fb.group({
        number: [{ value: '', disabled: false, validator: Validators.required }]
      });
  }
  onStartSession() {
    this.appService.startSession()
    .subscribe(data=> this.session = data);
  }

  onCreateSessionRequest() {
    this.currentRequest = new SessionRequest(this.session.id, this.session.user);
    this.appService.createSessionRequest(this.currentRequest)
    .subscribe(data=> {
      this.session.requests.push(data);
      this.currentRequest = data;
      this.formGroup.controls.number.setValue("");
    });
  }

  onCreateUserRequest() {
    this.currentRequest.userRequest = new UserRequest(this.currentRequest.user, this.formGroup.value.number);
    this.appService.updateSessionRequest(this.currentRequest)
    .subscribe(data=>{
      this.session = data;
      this.currentRequest = undefined;  
    });
    
  }

  private registryIcons() {
    this.iconRegistry.addSvgIcon(
      'label',
      this.sanitizer.bypassSecurityTrustResourceUrl('assets/img/ic_label_black_24px.svg'));
    this.iconRegistry.addSvgIcon(
      'face',
      this.sanitizer.bypassSecurityTrustResourceUrl('assets/img/ic_face_black_24px.svg'));
  }

  private fillExtrasenses() {
    this.appService.getAllExtrasenses()
      .subscribe(data => this.extrasenses = data);
  }
}
